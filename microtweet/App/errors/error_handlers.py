from flask import render_template
from App import db
from App.errors import blue_print


@blue_print.app_errorhandler(404)
def not_found_error(error):
    return render_template('errors/error_404.html'), 404


@blue_print.app_errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('errors/error_500.html'), 500
