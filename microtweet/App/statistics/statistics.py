from __future__ import annotations
import statistics
from abc import ABC, abstractmethod


class Statistic:
    def __init__(self, algorithm: Algorithm):
        self._algorithm = algorithm

    @property
    def algorithm(self):
        return self._algorithm

    @algorithm.setter
    def algorithm(self, algorithm: Algorithm):
        self._algorithm = algorithm

    def get_calculation_result(self):
        return self._algorithm.calculate()


class Algorithm(ABC):
    def __init__(self, data: list):
        self._data = data

    @abstractmethod
    def calculate(self):
        pass


class PostsAlg(Algorithm):
    def __init__(self, data):
        super().__init__(data)

    def calculate(self) -> int:
        posts = self._data[0]
        return len(posts)


class MsgAlg(Algorithm):
    def __init__(self, data):
        super().__init__(data)

    def calculate(self) -> int:
        msgs = self._data[1]
        return len(msgs)
