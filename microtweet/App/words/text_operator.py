import string
from wordcloud import ImageColorGenerator, WordCloud
from matplotlib import pyplot as plt
import numpy as np
from PIL import Image
import itertools
from pathlib import Path


file_location = "/Users/reckony/Desktop/UGSTUDIA/wzorce_projektowe/microblog_final/App/static/"
mask_image = "/Users/reckony/Desktop/UGSTUDIA/wzorce_projektowe/microblog_final/App/static/color.png"
noise = string.punctuation


class WordcloudGenerator:

    @staticmethod
    def create_wordcloud(username: str, user_posts_text: str) -> str:
        img_location = file_location + f'wordcloud_{username}.png'
        wordcloud_name = f'wordcloud_{username}.png'

        if Path(img_location).is_file():
            return '/' + wordcloud_name

        mask = np.array(Image.open(mask_image))
        user_wordcloud = WordCloud(max_font_size=50, background_color="white",
                                        mask=mask, contour_color='black', mode="RGBA").generate(user_posts_text)
        image_colors = ImageColorGenerator(mask)
        plt.figure(figsize=[7, 7])
        plt.imshow(user_wordcloud.recolor(color_func=image_colors), interpolation="bilinear")
        plt.axis("off")

        plt.savefig(img_location, format="png")
        plt.close('all')

        return '/' + wordcloud_name


class PostTextHandler:
    """
    Class helping to prepare data for wordcloud generation
    user_posts_dict:  {username: [Post, Post, Post]}
    """
    def __init__(self, user_posts_dict: dict):
        self.user_posts = user_posts_dict
        self.user = list(user_posts_dict)[0]
        self.posts_body_list = self.get_posts_body()
        self.posts_joined = PostTextHandler.join_posts(self.posts_body_list)

    def get_posts_body(self):
        posts = list(self.user_posts.values())
        posts_body = list()
        for post in posts:
            for some in post:
                post_join = ''.join(some.body)
                split_words = post_join.split(' ')
                posts_body.append(split_words)
        posts_body = list(itertools.chain.from_iterable(posts_body))
        posts_body = PostTextHandler.clear_data(posts_body)

        return posts_body

    @staticmethod
    def join_posts(posts: list):
        posts.sort()
        joined = ''
        for word in posts:
            joined = joined + ' ' + word
        return joined

    @staticmethod
    def clear_data(posts: list) -> list:
        cleared_data = [x for x in posts if x not in noise or len(x) > 3]
        return cleared_data




# na wejsciu {username: [post, post, post]}
# wziac post body
# zrobic w jeden string
# oczyscic
# zliczyc wystapienia {slowo: ilosc}
# do wordcloud {username: {slowo: ilosc, slowo: ilosc, slowo: ilosc, slowo: ilosc, itd}}