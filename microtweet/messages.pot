# Translations template for PROJECT.
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2020-05-31 19:38+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: App/__init__.py:19
msgid "Please log in to access this page."
msgstr ""

#: App/auth/email.py:8
msgid "[Microtweet] Reset Your Password"
msgstr ""

#: App/auth/forms.py:9 App/auth/forms.py:16 App/main/forms.py:14
msgid "Username"
msgstr ""

#: App/auth/forms.py:10 App/auth/forms.py:18 App/auth/forms.py:41
msgid "Password"
msgstr ""

#: App/auth/forms.py:11
msgid "Remember Me"
msgstr ""

#: App/auth/forms.py:12 App/auth/routes.py:27 App/templates/auth/login.html:5
msgid "Sign In"
msgstr ""

#: App/auth/forms.py:17 App/auth/forms.py:36
msgid "Email"
msgstr ""

#: App/auth/forms.py:20 App/auth/forms.py:43
msgid "Repeat Password"
msgstr ""

#: App/auth/forms.py:22 App/auth/routes.py:48
#: App/templates/auth/register.html:5
msgid "Register"
msgstr ""

#: App/auth/forms.py:27 App/main/forms.py:27
msgid "Please use a different username."
msgstr ""

#: App/auth/forms.py:32
msgid "Please use a different email address."
msgstr ""

#: App/auth/forms.py:37 App/auth/forms.py:45
msgid "Request Password Reset"
msgstr ""

#: App/auth/routes.py:20
msgid "Invalid username or password"
msgstr ""

#: App/auth/routes.py:46
msgid "You have been successfully registered."
msgstr ""

#: App/auth/routes.py:60
msgid "Check your email for the instructions to reset your password"
msgstr ""

#: App/auth/routes.py:62 App/templates/auth/reset_password_req.html:5
msgid "Reset Password"
msgstr ""

#: App/auth/routes.py:76
msgid "Your password has been reset."
msgstr ""

#: App/main/forms.py:9
msgid "How's your day?"
msgstr ""

#: App/main/forms.py:10 App/main/forms.py:17 App/main/forms.py:38
msgid "Submit"
msgstr ""

#: App/main/forms.py:15
msgid "About me"
msgstr ""

#: App/main/forms.py:32
msgid "Search"
msgstr ""

#: App/main/forms.py:36
msgid "Message"
msgstr ""

#: App/main/routes.py:35
msgid "Your post is now live!"
msgstr ""

#: App/main/routes.py:43 App/templates/base.html:18
msgid "Home"
msgstr ""

#: App/main/routes.py:58 App/templates/base.html:19
msgid "Explore"
msgstr ""

#: App/main/routes.py:84
msgid "Your changes have been saved."
msgstr ""

#: App/main/routes.py:89 App/templates/edit_profile.html:5
msgid "Edit Profile"
msgstr ""

#: App/main/routes.py:97 App/main/routes.py:113 App/main/routes.py:131
#, python-format
msgid "User %(username)s not found."
msgstr ""

#: App/main/routes.py:100
msgid "You cannot follow yourself."
msgstr ""

#: App/main/routes.py:104
#, python-format
msgid "You are following %(username)s!"
msgstr ""

#: App/main/routes.py:116
msgid "You cannot unfollow yourself."
msgstr ""

#: App/main/routes.py:120
#, python-format
msgid "You are not following %(username)s."
msgstr ""

#: App/main/routes.py:161
msgid "Your message has been sent."
msgstr ""

#: App/main/routes.py:163
msgid "Send Message"
msgstr ""

#: App/templates/_post.html:16
#, python-format
msgid "%(username)s said %(when)s"
msgstr ""

#: App/templates/base.html:4
msgid "Welcome to Microtweet"
msgstr ""

#: App/templates/base.html:22 App/templates/search_user.html:5
msgid "Search User"
msgstr ""

#: App/templates/base.html:27
msgid "Login"
msgstr ""

#: App/templates/base.html:31 App/templates/messages.html:4
msgid "Messages"
msgstr ""

#: App/templates/base.html:38
msgid "Profile"
msgstr ""

#: App/templates/base.html:39
msgid "Logout"
msgstr ""

#: App/templates/index.html:5
#, python-format
msgid "Hi, %(username)s!"
msgstr ""

#: App/templates/index.html:17 App/templates/user.html:34
msgid "Newer posts"
msgstr ""

#: App/templates/index.html:22 App/templates/user.html:39
msgid "Older posts"
msgstr ""

#: App/templates/messages.html:12
msgid "Newer messages"
msgstr ""

#: App/templates/messages.html:17
msgid "Older messages"
msgstr ""

#: App/templates/send_message.html:5
#, python-format
msgid "Send Message to %(recipient)s"
msgstr ""

#: App/templates/user.html:8
msgid "User"
msgstr ""

#: App/templates/user.html:11 App/templates/user_popup.html:13
msgid "Last seen on"
msgstr ""

#: App/templates/user.html:13 App/templates/user_popup.html:16
#, python-format
msgid "%(count)d followers"
msgstr ""

#: App/templates/user.html:13 App/templates/user_popup.html:17
#, python-format
msgid "%(count)d following"
msgstr ""

#: App/templates/user.html:15
msgid "Edit your profile"
msgstr ""

#: App/templates/user.html:17 App/templates/user_popup.html:21
msgid "Follow"
msgstr ""

#: App/templates/user.html:19 App/templates/user_popup.html:25
msgid "Unfollow"
msgstr ""

#: App/templates/user.html:22
msgid "Send private message"
msgstr ""

#: App/templates/auth/login.html:12
msgid "New User?"
msgstr ""

#: App/templates/auth/login.html:12
msgid "Click to Register!"
msgstr ""

#: App/templates/auth/login.html:14
msgid "Forgot Your Password?"
msgstr ""

#: App/templates/auth/login.html:15
msgid "Click to Reset It"
msgstr ""

#: App/templates/auth/reset_password.html:5
msgid "Reset Your Password"
msgstr ""

#: App/templates/errors/error_404.html:4
msgid "Not Found"
msgstr ""

#: App/templates/errors/error_404.html:5 App/templates/errors/error_500.html:6
msgid "Back"
msgstr ""

#: App/templates/errors/error_500.html:4
msgid "An unexpected error has occurred"
msgstr ""

#: App/templates/errors/error_500.html:5
msgid "The administrator has been notified. Sorry for the inconvenience."
msgstr ""

